package org.p2p.solanaj.rpc.types;

import com.squareup.moshi.Json;

public class RpcCommitmentConfig {

    public static enum Commitment {
        finalized("finalized"),
        confirmed("confirmed"),
        processed("processed");

        private String status;

        Commitment(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

    }

    @Json(name = "commitment")
    private Commitment commitment = Commitment.finalized;

    public RpcCommitmentConfig(Commitment commitment) {
        this.commitment = commitment;
    }
}
