package org.p2p.solanaj.rpc.types;

import com.squareup.moshi.Json;

public class Fee {

  public static class FeeContext {

    @Json(name = "slot")
    private long slot;

    public long getSlot() {
      return slot;
    }
  }

  public static class FeeValue {

    @Json(name = "blockhash")
    private String blockhash;

    @Json(name = "feeCalculator")
    private FeeCalculator feeCalculator;

    @Json(name = "lastValidSlot")
    private long lastValidSlot;

    @Json(name = "lastValidBlockHeight")
    private long lastValidBlockHeight;

    public String getBlockhash() {
      return blockhash;
    }

    public long getLastValidSlot() {
      return lastValidSlot;
    }

    public long getLastValidBlockHeight() {
      return lastValidBlockHeight;
    }

    public FeeCalculator getFeeCalculator() {
      return feeCalculator;
    }
  }

  public static class FeeCalculator {
    @Json(name = "lamportsPerSignature")
    private long lamportsPerSignature;

    public long getLamportsPerSignature() {
      return lamportsPerSignature;
    }
  }

  @Json(name = "context")
  private FeeContext context;

  @Json(name = "value")
  private FeeValue value;

  public FeeContext getContext() {
    return context;
  }

  public FeeValue getValue() {
    return value;
  }
}
