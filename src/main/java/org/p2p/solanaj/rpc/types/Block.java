package org.p2p.solanaj.rpc.types;

import com.squareup.moshi.Json;

import java.util.List;

public class Block {

  public static class BlockTransaction {

    @Json(name = "transaction")
    private ConfirmedTransaction.Transaction transaction;
    @Json(name = "meta")
    private ConfirmedTransaction.Meta meta = null;

    public ConfirmedTransaction.Transaction getTransaction() {
      return transaction;
    }

    public ConfirmedTransaction.Meta getMeta() {
      return meta;
    }
  }

  @Json(name = "blockTime")
  private long blockTime;

  @Json(name = "blockHeight")
  private long blockHeight;

  @Json(name = "blockhash")
  private String blockhash;

  @Json(name = "previousBlockhash")
  private String previousBlockhash;

  @Json(name = "parentSlot")
  private long parentSlot;

  @Json(name = "transactions")
  private List<BlockTransaction> transactions;

  @Json(name = "signatures")
  private List<String> signatures;


  public long getBlockTime() {
    return blockTime;
  }

  public long getBlockHeight() {
    return blockHeight;
  }

  public String getBlockhash() {
    return blockhash;
  }

  public String getPreviousBlockhash() {
    return previousBlockhash;
  }

  public long getParentSlot() {
    return parentSlot;
  }

  public List<BlockTransaction> getTransactions() {
    return transactions;
  }

  public List<String> getSignatures() {
    return signatures;
  }
}
