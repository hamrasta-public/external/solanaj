package org.p2p.solanaj.rpc.types;

import com.squareup.moshi.Json;

public class RpcBlockTransactionDetailConfig {

    public static enum TransactionDetail {
        full("full"),
        signatures("signatures"),
        none("none");

        private String level;

        TransactionDetail(String level) {
            this.level = level;
        }

        public String getLevel() {
            return level;
        }

    }

    @Json(name = "transactionDetails")
    private TransactionDetail transactionDetails = TransactionDetail.full;

    public RpcBlockTransactionDetailConfig(TransactionDetail transactionDetail) {
        this.transactionDetails = transactionDetail;
    }
}
