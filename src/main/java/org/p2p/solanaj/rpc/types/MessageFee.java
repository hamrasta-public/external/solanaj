package org.p2p.solanaj.rpc.types;

import com.squareup.moshi.Json;

public class MessageFee {

  public static class FeeContext {

    @Json(name = "slot")
    private long slot;

    public long getSlot() {
      return slot;
    }
  }

  @Json(name = "context")
  private FeeContext context;

  @Json(name = "value")
  private long value;

  public FeeContext getContext() {
    return context;
  }

  public long getValue() {
    return value;
  }
}
