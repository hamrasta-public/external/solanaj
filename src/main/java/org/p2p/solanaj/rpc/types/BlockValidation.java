package org.p2p.solanaj.rpc.types;

import com.squareup.moshi.Json;

public class BlockValidation {

  public static class Context {

    @Json(name = "slot")
    private long slot;

    public long getSlot() {
      return slot;
    }
  }

  @Json(name = "context")
  private Context context;

  @Json(name = "value")
  private Boolean value;

  public Context getContext() {
    return context;
  }

  public Boolean getValue() {
    return value;
  }
}
